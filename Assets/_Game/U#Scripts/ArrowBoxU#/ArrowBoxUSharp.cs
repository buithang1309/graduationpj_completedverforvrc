﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class ArrowBoxUSharp : UdonSharpBehaviour
{
    [SerializeField] ArrowBoxControllerUSharp arrowBoxController;

    [UdonSynced] public Vector3 currentRotation;
    public int arrowBoxPoint;
    [UdonSynced] public int syncedPoint;
    [SerializeField] bool isLateJoiner;

    private void Start()
    { 
        if (Networking.LocalPlayer.isMaster)
        {
            currentRotation = this.transform.rotation.eulerAngles;
        }
    }

    public void RotateBox()
    {
        transform.rotation *= Quaternion.Euler(0f, 0f, -90f);
        
        if (arrowBoxPoint == 4)
        {
            arrowBoxPoint = 1;
        }
        else
        {
            arrowBoxPoint++;
        }

        arrowBoxController.CheckPassword();

        //sync for late joiner
        currentRotation = this.transform.rotation.eulerAngles;
        syncedPoint = arrowBoxPoint;

        
        /*if (Networking.LocalPlayer.isMaster)
        {
            currentRotation = this.transform.rotation.eulerAngles;
            syncedPoint = arrowBoxPoint;
            RequestSerialization();
        }*/
        
    }

    public void Interact()
    {
        SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "RotateBox");
        //RotateBox();
    }

    //for late joiner
    public override void OnDeserialization()
    {
        if (isLateJoiner)
        {
            isLateJoiner = false;
            LateJoinerCheck();
        }
        
    }

    void LateJoinerCheck()
    {
        this.transform.rotation = Quaternion.Euler(currentRotation);
        arrowBoxPoint = syncedPoint;
    }
}
