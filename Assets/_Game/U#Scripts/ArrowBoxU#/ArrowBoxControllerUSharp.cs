﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class ArrowBoxControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] ArrowBoxUSharp[] arrowBoxes;
    [SerializeField] GameObject reward;
    [SerializeField] [UdonSynced] bool done;
    [SerializeField] AudioSource soundFX;

    public string arrowBoxPassword = "41223344";

    private void Start()
    {
        if (Networking.LocalPlayer.isMaster)
        {
            done = false;
        }
    }

    public void CheckPassword()
    {
        string checkingPass = "";
        for (int i = 0; i < arrowBoxes.Length; i++)
        {
            checkingPass += arrowBoxes[i].arrowBoxPoint.ToString();
        }
        Debug.Log(checkingPass);
        if (checkingPass == arrowBoxPassword)
        {
            FinishGame();
        }
    }

    public void FinishGame()
    {
        if (!done)
        {
            soundFX.Play();
            done = true;
            reward.SetActive(true);
            //this.gameObject.SetActive(false);
        }
    }

    public override void OnDeserialization()
    {
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            reward.SetActive(true);
        }
    }
}
