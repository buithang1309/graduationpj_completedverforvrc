﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class SwitchBoxControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] [UdonSynced] bool done;
    [SerializeField] SwitchBoxUSharp[] switchBoxes;
    [SerializeField] bool isFinish;
    [SerializeField] public Material matOn;
    [SerializeField] public Material matOff;
    [SerializeField] GameObject reward;
    [SerializeField] AudioSource soundFX;

    private void Start()
    {
        if (Networking.LocalPlayer.isMaster)
        {
            done = false;
        }
    }

    public void CheckFinish()
    {
        isFinish = true;
        foreach (var s in switchBoxes)
        {
            if (!s.switchStatus)
            {
                isFinish = false;
            }
        }

        if (isFinish)
        {
            FinishGame();
        }
    }

    public void ResetPuzzle()
    {
        for (int i = 0; i < switchBoxes.Length; i++)
        {
            if (i == 3 || i == 7)
            {
                switchBoxes[i].switchStatus = true;
                switchBoxes[i].boxMat.material = matOn;

                switchBoxes[i].syncedSwitch = switchBoxes[i].switchStatus;
            }
            else
            {
                switchBoxes[i].switchStatus = false;
                switchBoxes[i].boxMat.material = matOff;

                switchBoxes[i].syncedSwitch = switchBoxes[i].switchStatus;
            }
        }
    }

    void FinishGame()
    {
        if (!done)
        {
            soundFX.Play();
            done = true;
            reward.SetActive(true);
            //this.gameObject.SetActive(false);
        }
    }

    //for late joiner
    public override void OnDeserialization()
    {
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            reward.SetActive(true);
        }
    }
}
