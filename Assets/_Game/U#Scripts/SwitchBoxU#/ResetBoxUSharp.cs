﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class ResetBoxUSharp : UdonSharpBehaviour
{
    [SerializeField] SwitchBoxControllerUSharp switchBoxController;

    public void Interact()
    {
        switchBoxController.SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "ResetPuzzle");
        switchBoxController.ResetPuzzle();
    }
}
