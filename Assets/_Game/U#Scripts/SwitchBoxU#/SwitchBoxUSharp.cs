﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class SwitchBoxUSharp : UdonSharpBehaviour
{
    [SerializeField] public bool switchStatus;
    [SerializeField] [UdonSynced] public bool canWork;
    [SerializeField] SwitchBoxUSharp[] boxes;
    [SerializeField] public SwitchBoxControllerUSharp switchBoxController;
    [SerializeField] public Renderer boxMat;

    //for late joiner
    [SerializeField] bool isLateJoiner;
    [SerializeField] [UdonSynced] public bool syncedSwitch;

    public void Switch()
    {
        if (canWork)
        {
            switchStatus = !switchStatus;
            if (switchStatus)
            {
                boxMat.material = switchBoxController.matOn;
            }
            else
            {
                boxMat.material = switchBoxController.matOff;
            }

            //sync udon variable for late joiner
            syncedSwitch = switchStatus;
            
            /*if (Networking.LocalPlayer.isMaster)
            {
                
                //RequestSerialization();
            }*/
        }
    }

    public void ClickBox()
    {
        if (canWork)
        {
            foreach (var b in boxes)
            {
                b.Switch();
            }

            switchBoxController.CheckFinish();
        }
    }

    public void EnableComponent()
    {
        this.GetComponent<VRC_Pickup>().Drop();
        this.GetComponent<VRC_Pickup>().pickupable = false;
        this.GetComponent<Rigidbody>().isKinematic = true;

        if (!canWork)
        {
            canWork = true;
            //switchBoxController.ResetPuzzle();
            switchBoxController.SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "ResetPuzzle");
        }
    }

    public void Interact()
    {
        SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "ClickBox");
    }

    //for late joiner
    public override void OnDeserialization()
    {
        if (isLateJoiner)
        {
            isLateJoiner = false;
            LateJoinerCheck();
        }
    }

    void LateJoinerCheck()
    {
        if (syncedSwitch)
        {
            boxMat.material = switchBoxController.matOn;
        }
        else
        {
            boxMat.material = switchBoxController.matOff;
        }

        switchStatus = syncedSwitch;
    }
}
