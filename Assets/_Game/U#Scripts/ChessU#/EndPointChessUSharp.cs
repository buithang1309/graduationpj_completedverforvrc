﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class EndPointChessUSharp : UdonSharpBehaviour
{
    public VRC.SDKBase.VRCPlayerApi player;
    [SerializeField] ChessPuzzleControllerUSharp chessPuzzleController;

    void OnPlayerTriggerEnter(VRC.SDKBase.VRCPlayerApi player)
    {
        this.player = player;
        chessPuzzleController.FinishGame();
    }
}
