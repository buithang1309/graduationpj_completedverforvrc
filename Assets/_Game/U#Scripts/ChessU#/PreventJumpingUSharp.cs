﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class PreventJumpingUSharp : UdonSharpBehaviour
{
    public float disabledJumpImpulse = 0f; // The jump impulse when inside the trigger
    public float normalJumpImpulse = 4f; // The normal jump impulse

    public float disabledWalkSpeed = 2f;
    public float normalWalkSpeed = 3f;

    public float disabledRunSpeed = 2f;
    public float normalRunSpeed = 6f;

    //this is local trigger
    public override void OnPlayerTriggerStay(VRCPlayerApi player)
    {
        if (player == Networking.LocalPlayer) // Check if the player is the local player
        {
            player.SetJumpImpulse(disabledJumpImpulse); // Set the jump impulse
            player.SetWalkSpeed(disabledWalkSpeed);
            player.SetRunSpeed(disabledRunSpeed);
        }
    }

    public override void OnPlayerTriggerExit(VRCPlayerApi player)
    {
        if (player == Networking.LocalPlayer) // Check if the player is the local player
        {
            player.SetJumpImpulse(normalJumpImpulse); // Restore the normal jump impulse
            player.SetWalkSpeed(normalWalkSpeed);
            player.SetRunSpeed(normalRunSpeed);
        }
    }
}
