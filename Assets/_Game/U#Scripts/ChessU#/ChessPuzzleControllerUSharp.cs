﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class ChessPuzzleControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] GameObject liveGround;
    [SerializeField] GameObject chessItem;
    [SerializeField] GameObject endPointChess;
    [SerializeField] GameObject reward;
    [SerializeField] AudioSource soundFX;
    [SerializeField] [UdonSynced] bool done;

    private void Start()
    {
        if (Networking.LocalPlayer.isMaster)
        {
            done = false;
        }
    }

    public void FinishGame()
    {
        done = true;
        soundFX.Play();
        chessItem.SetActive(false);
        endPointChess.SetActive(false);
        liveGround.SetActive(true);
        reward.SetActive(true);
    }

    //for late joiner
    public override void OnDeserialization()
    {
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            chessItem.SetActive(false);
            endPointChess.SetActive(false);
            liveGround.SetActive(true);
            reward.SetActive(true);
        }
    }
}
