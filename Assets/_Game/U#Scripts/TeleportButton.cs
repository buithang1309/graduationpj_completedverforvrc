﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class TeleportButton : UdonSharpBehaviour
{
    [SerializeField] Transform telePoint;
    public void Interact()
    {
        Networking.LocalPlayer.TeleportTo(telePoint.position, telePoint.rotation);
    }
}
