﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class ClockPuzzleControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] [UdonSynced] bool done;
    [UdonSynced] public int statusCount;
    [SerializeField] GameObject reward;
    [SerializeField] AudioSource soundFX;

    private void Start()
    {
        if (Networking.LocalPlayer.isMaster)
        {
            statusCount = 1;
            done = false;
        }
    }

    public void CheckStatus()
    {
        if (statusCount == 4 && !done)
        {
            soundFX.Play();
            done = true;
            reward.SetActive(true);
            //this.gameObject.SetActive(false);
        }
    }

    //for late joiner
    public override void OnDeserialization()
    {
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            reward.SetActive(true);
        }
    }
}
