﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class ClockInteractUSharp : UdonSharpBehaviour
{
    [SerializeField] [UdonSynced] bool canWork;
    [SerializeField] bool clockStatus;
    [SerializeField] Transform hourNeedle;
    [SerializeField] int currentHour;
    [SerializeField] int correctHour;
    [SerializeField] [UdonSynced] int syncedHour;
    [SerializeField] ClockPuzzleControllerUSharp clockPuzzleController;
    [SerializeField] bool isLateJoiner;
    [SerializeField] [UdonSynced] bool syncedStatus;

    public void UpdateClockTime()
    {
        if (canWork)
        {
            hourNeedle.localRotation *= Quaternion.Euler(-30f, 0f, 0f);
            currentHour++;
            if (currentHour == 12)
            {
                currentHour = 0;
            }

            if (clockPuzzleController.statusCount != 4)
            {
                if (currentHour == correctHour)
                {
                    clockStatus = true;
                    clockPuzzleController.statusCount++;
                }
                else if (clockStatus)
                {
                    clockStatus = false;
                    clockPuzzleController.statusCount--;
                }
            }

            //sync udon variable for late joiner
            /*if (Networking.LocalPlayer.isMaster)
            {
                syncedHour = currentHour;
                syncedStatus = clockStatus;
                RequestSerialization();
            }*/

            clockPuzzleController.CheckStatus();

            syncedHour = currentHour;
            syncedStatus = clockStatus;
        }
    }

    public void EnableComponent()
    {
        this.GetComponent<VRC_Pickup>().Drop();
        this.GetComponent<VRC_Pickup>().pickupable = false;
        this.GetComponent<Rigidbody>().isKinematic = true;
        if (!canWork)
        {
            canWork = true;
        }
    }

    public void Interact()
    {
        SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "UpdateClockTime");
    }

    public override void OnDeserialization()
    {
        if (isLateJoiner)
        {
            isLateJoiner = false;
            LateJoinerCheck();
        }
    }

    void LateJoinerCheck()
    {
        hourNeedle.localRotation = Quaternion.Euler(-90f -30f * syncedHour, 0f, 0f);
        //hourNeedle.localRotation *= Quaternion.Euler(-30f * syncedHour, 0f, 0f);

        currentHour = syncedHour;
        clockStatus = syncedStatus;
    }
}
