﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using TMPro;

public class ColorBoxUSharp : UdonSharpBehaviour
{
    [SerializeField] public MeshRenderer boxMat;
    [SerializeField] public TextMeshPro boxNumbers;
}
