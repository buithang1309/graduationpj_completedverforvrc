﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
//using VRC.Udon.Common.Interfaces;

public class ColorBoxControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] [UdonSynced] bool done;
    [SerializeField] ColorBoxUSharp[] colorBoxes;
    [SerializeField] [UdonSynced] int[] shuffledIndices = new int[7];
    [SerializeField] Material[] boxMaterials;

    [SerializeField] [UdonSynced] int[] passwordArray = new int[7];
    [SerializeField] [UdonSynced] string passwordString;

    [SerializeField] KeypadControllerUSharp keypad;
    [SerializeField] GameObject reward;
    
    [SerializeField] AudioSource soundFX;
    private void Start()
    {
        //CreatePuzzle();
        if (Networking.LocalPlayer.isMaster)
        {
            done = false;
            CreatePuzzle();
        }
    }

    public void CreatePuzzle()
    {
        ShuffleArray();
        PasswordGenerator();
        SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "AssignColorPassword");
    }

    public void PasswordGenerator()
    {
        for (int i = 0; i < passwordArray.Length; i++)
        {
            passwordArray[i] = Random.Range(0, 10);
        }

        //string passwordString = string.Join("", passwordArray);
        //AssignColor();
    }

    public void AssignColorPassword()
    {
        for (int i = 0; i < colorBoxes.Length; i++)
        {
            colorBoxes[shuffledIndices[i]].boxMat.material = boxMaterials[i];
            colorBoxes[shuffledIndices[i]].boxNumbers.text = passwordArray[i].ToString();
        }

        passwordString = "";

        for (int i = 0; i < passwordArray.Length; i++)
        {
            passwordString += passwordArray[i].ToString();
        }
        keypad.GetPassword(passwordString);
    }

    /*void AssignColor()
    {
        for (int i = 0; i < colorBoxes.Length; i++)
        {
            colorBoxes[i].boxMat.material = boxMaterials[i];
            colorBoxes[i].boxNumbers.text = passwordArray[i].ToString();
        }
    }*/

    void ShuffleArray()
    {
        int n = colorBoxes.Length;
        for (int i = 0; i < n; i++)
        {
            // Generate a random index within the remaining unshuffled portion of the array
            int randomIndex = Random.Range(i, n);

            // Swap the current element with the randomly chosen element
            int temp = shuffledIndices[i];
            shuffledIndices[i] = shuffledIndices[randomIndex];
            shuffledIndices[randomIndex] = temp;
        }
    }

    /*void ShuffleArray(ColorBoxUSharp[] colorBoxes)
    {
        int n = colorBoxes.Length;
        for (int i = 0; i < n; i++)
        {
            // Generate a random index within the remaining unshuffled portion of the array
            int randomIndex = Random.Range(i, n);

            // Swap the current element with the randomly chosen element
            ColorBoxUSharp temp = colorBoxes[i];
            colorBoxes[i] = colorBoxes[randomIndex];
            colorBoxes[randomIndex] = temp;
        }
    }*/

    public void PasswordCorrect()
    {
        FinishGame();
    }

    public void FinishGame()
    {
        if (!done)
        {
            soundFX.Play();
            done = true;
            reward.SetActive(true);
            //this.gameObject.SetActive(false);
        }

    }

    //for late joiner
    public override void OnDeserialization()
    {
        AssignColorPassword();
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            reward.SetActive(true);
        }
    }
}
