﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class TeleportAllPlayers : UdonSharpBehaviour
{
    // World capacity is 10, so we create a new array with length of 20 (Hard cap)  
    VRCPlayerApi[] players = new VRCPlayerApi[20];
    [SerializeField] Transform targetPosition;

    void Start()
    {
        VRCPlayerApi.GetPlayers(players);

        foreach (VRCPlayerApi player in players)
        {
            if (player == null) continue;
            player.TeleportTo(targetPosition.position,
            targetPosition.rotation,
            VRC_SceneDescriptor.SpawnOrientation.Default,
            false);
        }
    }
}
