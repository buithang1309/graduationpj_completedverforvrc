﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class SnapPointUSharp : UdonSharpBehaviour
{
    public UdonBehaviour program;
    private void OnTriggerEnter(Collider other)
    {
        var obj = other.GetComponent<SnapObject>();
        if (obj != null)
        {
            if(obj.snapPointUSharp == this)
            {
                program = obj.GetComponent<UdonBehaviour>();
                program.SendCustomEvent("EnableComponent");
                obj.transform.position = this.transform.position;
                obj.transform.rotation = this.transform.rotation;
                obj.transform.SetParent(this.transform.parent);
                this.gameObject.SetActive(false);
            }
        }
    }
}
