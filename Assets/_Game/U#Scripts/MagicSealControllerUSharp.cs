﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class MagicSealControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] GameObject magicSeal;
    [SerializeField] Renderer magicSealMat;
    [SerializeField] Light magicLight;
    [UdonSynced] Color tempValue;
    [SerializeField] [UdonSynced] bool done;
    [SerializeField] [UdonSynced] float elapsedTime;
    [SerializeField] AudioSource soundFX;
    private void Start()
    {
        if (Networking.LocalPlayer.isMaster)
        {
            done = false;
            tempValue = Color.white;
            elapsedTime = 0f;
        }  
    }

    private void Update()
    {
        magicSeal.transform.Rotate(Vector3.forward * 40 * Time.deltaTime);

        if (done)
        {
            if (elapsedTime < 5f)
            {
                float matValue = Mathf.Lerp(1, 0, elapsedTime / 5f);
                float lightValue = Mathf.Lerp(5, 0, elapsedTime / 5f);
                tempValue.a = matValue;
                magicSealMat.material.color = tempValue;
                magicLight.intensity = lightValue;
                elapsedTime += Time.deltaTime;
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }
    }


    public void PasswordCorrect()
    {
        FinishGame();
    }

    public void FinishGame()
    {
        if (!done)
        {
            done = true;
            soundFX.Play();
        }
    }

    //for late joiner
    public override void OnDeserialization()
    {
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            this.gameObject.SetActive(false);
        }
    }
}
