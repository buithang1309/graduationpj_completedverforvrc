﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class PostProcessingToggle : UdonSharpBehaviour
{
    [SerializeField] GameObject postProcess;
    public void Interact()
    {
        postProcess.SetActive(!postProcess.activeInHierarchy);
    }
}
