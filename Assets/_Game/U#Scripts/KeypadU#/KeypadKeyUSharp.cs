﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class KeypadKeyUSharp : UdonSharpBehaviour
{
    public string key;

    public void SendKey()
    {
        this.transform.GetComponentInParent<KeypadControllerUSharp>().PasswordEntry(key);
    }

    public void Interact()
    {
        SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "SendKey");
        //SendKey();
    }
}
