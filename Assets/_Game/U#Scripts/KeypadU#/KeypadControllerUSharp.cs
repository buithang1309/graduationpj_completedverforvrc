﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;

public class KeypadControllerUSharp : UdonSharpBehaviour
{
    public string password;
    public int passwordLimit;
    public Text passwordText;

    [Header("Audio")]
    public AudioSource audioSource;
    public AudioClip correctSound;
    public AudioClip wrongSound;

    [Header("Type")]
    public UdonBehaviour program;
    public Text showingText;
    [SerializeField] [UdonSynced] bool isLateJoiner;

    private void Start()
    {
        passwordText.text = "";
    }

    public void PasswordEntry(string number)
    {
        if (number == "Clear")
        {
            Clear();
            return;
        }
        else if (number == "Enter")
        {
            Enter();
            return;
        }

        int length = passwordText.text.ToString().Length;
        if (length < passwordLimit)
        {
            passwordText.text = passwordText.text + number;
        }
    }

    public void Clear()
    {
        passwordText.text = "";
        passwordText.color = Color.white;
    }

    private void Enter()
    {
        if (passwordText.text == password)
        {
            if (audioSource != null)
                audioSource.PlayOneShot(correctSound);

            passwordText.color = Color.green;
            OnPasswordCorrect();
        }
        else
        {
            if (audioSource != null)
                audioSource.PlayOneShot(wrongSound);

            passwordText.color = Color.red;
            SendCustomEventDelayedSeconds("Clear",0.75f);
        }
    }

    public void GetPassword(string newPassword)
    {
        password = newPassword;
        if (showingText != null)
        {
            showingText.text = password;
        }
    }

    public void OnPasswordCorrect()
    {
        if(program != null)
        {
            program.SendCustomEvent("PasswordCorrect");
        }
    }
}
