﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class RubikCoreUSharp : UdonSharpBehaviour
{
    public bool coreStatus = false;
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<RubikCubeUSharp>())
        {
            coreStatus = true;
            this.gameObject.GetComponent<Renderer>().material.color = Color.magenta;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<RubikCubeUSharp>())
        {
            coreStatus = false;
            this.gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
