﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class RubikPuzzleControllerUSharp : UdonSharpBehaviour
{
    [SerializeField] RubikCoreUSharp[] cores;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip winSound;
    [UdonSynced] public int corePoint;
    [SerializeField] GameObject buttonTele;
    [SerializeField] Transform endGameSpawn;
    [SerializeField] [UdonSynced] bool done;
    [SerializeField] GameObject endGameRoom;

    private void Start()
    {
        if (Networking.LocalPlayer.isMaster)
        {
            done = false;
            corePoint = 0;
        } 
    }

    private void Update()
    {
        if (corePoint != 27)
        {
            corePoint = 0;

            foreach (var core in cores)
            {
                if (core.coreStatus)
                {
                    corePoint++;
                }
            }

            if (corePoint == 27)
            {
                if (Networking.LocalPlayer.isMaster)
                {
                    SendCustomNetworkEvent(VRC.Udon.Common.Interfaces.NetworkEventTarget.All, "PlayEndSound");
                }
                //PlayEndSound();
            }
        }
    }

    public void PlayEndSound()
    {
        audioSource.PlayOneShot(winSound);
        SendCustomEventDelayedSeconds("FinishGame", 5f);
    }

    public void FinishGame()
    {
        if (!done)
        {
            endGameRoom.SetActive(true);
            buttonTele.SetActive(true);
            done = true;
            Networking.LocalPlayer.TeleportTo(endGameSpawn.position,endGameSpawn.rotation);
            //this.gameObject.SetActive(false);
        }
    }

    public override void OnDeserialization()
    {
        LateJoinerCheck();
    }

    void LateJoinerCheck()
    {
        if (done)
        {
            endGameRoom.SetActive(true);
            buttonTele.SetActive(true);
        }
    }
}
