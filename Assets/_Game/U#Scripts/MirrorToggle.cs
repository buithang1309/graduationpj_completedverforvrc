﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class MirrorToggle : UdonSharpBehaviour
{
    [SerializeField] GameObject mirrorLow;
    [SerializeField] GameObject mirrorHigh;
    [SerializeField] bool isLow;
    [SerializeField] bool isHigh;
    public void Interact()
    {
        if (isLow)
        {
            mirrorLow.SetActive(!mirrorLow.activeInHierarchy);
            mirrorHigh.SetActive(false);
        }

        if (isHigh)
        {
            mirrorLow.SetActive(false);
            mirrorHigh.SetActive(!mirrorHigh.activeInHierarchy);
        }
    }
}
