using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameController : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip winSound;
    [SerializeField] GameObject player;
    [SerializeField] Transform endPos;
    public void Winning()
    {
        StartCoroutine(EndGame());
    }
    IEnumerator EndGame()
    {
        audioSource.PlayOneShot(winSound);
        yield return new WaitForSeconds(5);
        player.transform.position = endPos.position;
    }
}
