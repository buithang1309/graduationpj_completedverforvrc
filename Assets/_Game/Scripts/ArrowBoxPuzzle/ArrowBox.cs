using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBox : MonoBehaviour
{
    [SerializeField] ArrowBoxController arrowBoxController;

    public int arrowBoxPoint = 1;

    private void Start()
    {
        arrowBoxPoint = 1;
    }

    public void RotateBox()
    {
        transform.rotation *= Quaternion.Euler(0f, 0f, -90f);
        if(arrowBoxPoint == 4)
        {
            arrowBoxPoint = 1;
        }
        else
        {
            arrowBoxPoint++;
        }

        arrowBoxController.CheckPassword();
    }
}
