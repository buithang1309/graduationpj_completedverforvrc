using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBoxController : MonoBehaviour
{
    [SerializeField] ArrowBox[] arrowBoxes;
    [SerializeField] GameObject reward;

    public string arrowBoxPassword = "41223344";
    
    public void CheckPassword()
    {
        string checkingPass = "";
        for(int i = 0; i < arrowBoxes.Length; i++)
        {
            checkingPass += arrowBoxes[i].arrowBoxPoint.ToString();
        }
        Debug.Log(checkingPass);
        if (checkingPass == arrowBoxPassword)
        {
            FinishGame();
        }
    }

    public void FinishGame()
    {
        reward.SetActive(true);
        Debug.Log("Finish game");
        this.gameObject.SetActive(false);
    }
}
