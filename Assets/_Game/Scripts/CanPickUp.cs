using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanPickUp : MonoBehaviour
{
    public bool isSwitchBox;
    public bool isClock;
    public bool isRubik;
    public SnapPoint snapPoint;
    public bool canHoldHand;

    public void EnableComponent()
    {
        if (isSwitchBox)
        {
            var switchBox = GetComponent<SwitchBox>();
            switchBox.enabled = true;
            switchBox.EnableSwitchBox();
        }
        if (isClock)
        {
            var clock = GetComponent<ClockInteract>();
            clock.enabled = true;
            clock.EnableClock();
        }
        if (isRubik)
        {
            GameController.Instance.UpdateRubikPuzzle();
        }
    }

    private void OnDisable()
    {
        GameController.Instance.QuickDrop();
    }
}
