using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupAndInteract : MonoBehaviour
{
    public GameObject player;
    public Transform holdPos;
    public Transform handHoldPos;
    public FirstPersonLook mouseLookScript;
    public Transform originParent;

    public float interactRange = 3f;
    private float rotationSensitivity = 1f;
    private GameObject heldObj;
    private bool canDrop = true;
    private bool isHandHolding;

    private CanPickUp hitObject;

    void Start()
    {
        isHandHolding = false;
    }

    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, interactRange))
        {
            if (Input.GetMouseButtonDown(0))
            {
                Interact(hit);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (heldObj == null)
            {
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, interactRange))
                {
                    hitObject = hit.transform.GetComponent<CanPickUp>();
                    if (hitObject != null && hitObject.isActiveAndEnabled)
                    {
                        Debug.Log("okok3");
                        if (hitObject.canHoldHand)
                        {
                            Debug.Log("okok4");
                            isHandHolding = true;
                            PickUpHolding(hit.transform.gameObject);
                        }
                        else
                        {
                            PickUpObject(hit.transform.gameObject);
                        }
                    }
                }
            }
            else
            {
                if (canDrop == true)
                {
                    StopClipping();
                    DropObject();
                }
            }
        }
        if (heldObj != null)
        {
            if (!isHandHolding)
            {
                RotateObject();
            }
            else
            {
                HandHold();
            }
        } 
    }
    #region Static Object Interact
    void Interact(RaycastHit hit)
    {
        if (hit.transform.GetComponent<KeypadKey>() != null)
        {
            hit.transform.GetComponent<KeypadKey>().SendKey();
        }

        if (hit.transform.GetComponent<ArrowBox>() != null)
        {
            hit.transform.GetComponent<ArrowBox>().RotateBox();
        }

        if (hit.transform.GetComponent<ClockInteract>() != null)
        {
            hit.transform.GetComponent<ClockInteract>().UpdateClockTime();
        }

        if (hit.transform.GetComponent<SwitchBox>() != null)
        {
            hit.transform.GetComponent<SwitchBox>().ClickBox();
        }

        if (hit.transform.GetComponent<ResetBox>() != null)
        {
            hit.transform.GetComponent<ResetBox>().ResetPuzzle();
        }

    }
    #endregion

    #region Object Pickup
    void PickUpObject(GameObject pickUpObj)
    {
        originParent = pickUpObj.transform.parent;
        heldObj = pickUpObj;

        Vector3 heldObjPos = heldObj.transform.position;
        holdPos.transform.position = heldObjPos;
        heldObj.transform.parent = holdPos.transform;

        Physics.IgnoreCollision(heldObj.GetComponent<Collider>(), player.GetComponent<Collider>(), true);
    }

    void PickUpHolding(GameObject pickUpObj)
    {
        originParent = pickUpObj.transform.parent;
        heldObj = pickUpObj; 

        heldObj.transform.parent = handHoldPos.transform; 
        heldObj.transform.position = handHoldPos.position;
        heldObj.transform.rotation = handHoldPos.rotation;
        Physics.IgnoreCollision(heldObj.GetComponent<Collider>(), player.GetComponent<Collider>(), true);
    }

    void HandHold()
    {
        heldObj.transform.position = handHoldPos.position;
    }

    public void DropObject()
    {
        Physics.IgnoreCollision(heldObj.GetComponent<Collider>(), player.GetComponent<Collider>(), false);

        heldObj.transform.parent = originParent; 
        heldObj = null;

        isHandHolding = false;
    }

    void RotateObject()
    {
        if (Input.GetKey(KeyCode.R))
        {
            canDrop = false; 

            mouseLookScript.sensitivity = 0;

            float XaxisRotation = Input.GetAxis("Mouse X") * rotationSensitivity;
            float YaxisRotation = Input.GetAxis("Mouse Y") * rotationSensitivity;

            heldObj.transform.Rotate(Vector3.down, XaxisRotation);
            heldObj.transform.Rotate(Vector3.right, YaxisRotation);
        }
        else
        {
            mouseLookScript.sensitivity = 2;
            canDrop = true;
        }
    }

    public void StopClipping()
    {
        var clipRange = Vector3.Distance(heldObj.transform.position, transform.position);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.TransformDirection(Vector3.forward), clipRange);
        if (hits.Length > 1)
        {

            heldObj.transform.position = transform.position + new Vector3(0f, -0.5f, 0f); 
        }
    }
    #endregion
    
}
