using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessPuzzleController : MonoBehaviour
{
    [SerializeField] EndPointChess endPoint;

    [SerializeField] Transform respawnPoint;

    [SerializeField] GameObject liveGround;
    [SerializeField] GameObject deathGround;

    [SerializeField] GameObject chessItem;
    [SerializeField] GameObject reward;

    public void Respawn(GameObject player)
    {
        player.transform.position = respawnPoint.position;
    }

    public void FinishGame()
    {
        chessItem.SetActive(false);
        liveGround.SetActive(true);
        reward.SetActive(true);
    }
}
