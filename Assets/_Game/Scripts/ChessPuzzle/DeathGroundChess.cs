using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathGroundChess : MonoBehaviour
{
    [SerializeField] ChessPuzzleController chessPuzzleController;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            chessPuzzleController.Respawn(other.gameObject);
        }
    }
}
