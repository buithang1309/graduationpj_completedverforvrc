using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CanPickUp>())
        {
            var pickedObj = other.GetComponent<CanPickUp>();
            if (pickedObj.snapPoint == this)
            {
                Debug.Log("true snap");
                pickedObj.EnableComponent();
                pickedObj.enabled = false;
                pickedObj.transform.position = this.transform.position;
                pickedObj.transform.rotation = this.transform.rotation;
                pickedObj.transform.SetParent(this.transform.parent);
                this.gameObject.SetActive(false);
            }
        }
    }
}
