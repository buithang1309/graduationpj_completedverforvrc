using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBox : MonoBehaviour
{
    [SerializeField] SwitchBoxController switchBoxController;
    public void ResetPuzzle()
    {
        switchBoxController.ResetPuzzle();
    }
}
