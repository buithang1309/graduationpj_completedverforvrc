using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBox : MonoBehaviour
{
    [SerializeField] public bool switchStatus;

    [SerializeField] SwitchBox[] boxes;

    [SerializeField] public SwitchBoxController switchBoxController;
    [SerializeField] public Renderer boxMat;
    [SerializeField] public bool canWork;
    public void Switch()
    {
        if (canWork)
        {
            switchStatus = !switchStatus;
            if (switchStatus)
            {
                boxMat.material = switchBoxController.matOn;
            }
            else
            {
                boxMat.material = switchBoxController.matOff;
            }
        }
    }

    public void ClickBox()
    {
        if (canWork)
        {
            foreach (var b in boxes)
            {
                b.Switch();
            }

            switchBoxController.CheckFinish();
        }
    }

    public void EnableSwitchBox()
    {
        canWork = true;
        switchBoxController.enabled = true;
    }
}
