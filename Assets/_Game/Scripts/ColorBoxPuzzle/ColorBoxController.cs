using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ColorBoxController : MonoBehaviour
{
    [SerializeField] ColorBox[] colorBoxes;
    [SerializeField] Material[] boxMaterials;

    [SerializeField] int[] passwordArray = new int[7];

    [SerializeField] KeypadController keypad;
    [SerializeField] GameObject reward;
    private void Start()
    {
        ShuffleArray(colorBoxes);
        PasswordGenerator();
        keypad.OnPasswordCorrect += FinishGame;
    }

    public void PasswordGenerator()
    {
        for (int i = 0; i < passwordArray.Length; i++)
        {
            passwordArray[i] = Random.Range(0, 10);
        }

        string passwordString = string.Join("", passwordArray);

        keypad.GetPassword(passwordString);

        AssignColor();
    }

    void AssignColor()
    {
        for(int i = 0; i < colorBoxes.Length; i++)
        {
            colorBoxes[i].boxMat.material = boxMaterials[i];
            colorBoxes[i].boxNumbers.text = passwordArray[i].ToString();
        }
    }

    void ShuffleArray(ColorBox[] colorBoxes)
    {
        int n = colorBoxes.Length;
        for (int i = 0; i < n; i++)
        {
            // Generate a random index within the remaining unshuffled portion of the array
            int randomIndex = Random.Range(i, n);

            // Swap the current element with the randomly chosen element
            ColorBox temp = colorBoxes[i];
            colorBoxes[i] = colorBoxes[randomIndex];
            colorBoxes[randomIndex] = temp;
        }
    }

    public void FinishGame()
    {
        Debug.Log("Finish");
        reward.SetActive(true);
        this.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        keypad.OnPasswordCorrect -= FinishGame;
    }
}
