using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ColorBox : MonoBehaviour
{
    [SerializeField] public MeshRenderer boxMat;
    [SerializeField] public TextMeshPro boxNumbers;
}
