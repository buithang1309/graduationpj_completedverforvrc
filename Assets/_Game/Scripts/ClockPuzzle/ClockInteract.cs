using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockInteract : MonoBehaviour
{
    [SerializeField] ClockPuzzleController clockPuzzleController;
    [SerializeField] bool clockStatus = false;
    [SerializeField] Transform hourNeedle;
    [SerializeField] Vector3 correctRotation;
    [SerializeField] bool canWork;

    public void UpdateClockTime()
    {
        if (canWork)
        {
            hourNeedle.rotation *= Quaternion.Euler(-30f, 0f, 0f);
            Debug.Log("hour needel:" + hourNeedle.rotation.eulerAngles);
            Debug.Log("correct:" + correctRotation);
            var dis = Vector3.Distance(hourNeedle.rotation.eulerAngles, correctRotation);
            Debug.Log("dis: " + dis);

            if (Vector3.Distance(hourNeedle.rotation.eulerAngles, correctRotation) <= 0.1f)
            {
                clockStatus = true;
                clockPuzzleController.statusCount++;
            }
            else if (clockStatus)
            {
                clockStatus = false;
                clockPuzzleController.statusCount--;
            }

            clockPuzzleController.CheckStatus();
        }
    }

    public void EnableClock()
    {
        canWork = true;
        clockPuzzleController.enabled = true;
    }
}
