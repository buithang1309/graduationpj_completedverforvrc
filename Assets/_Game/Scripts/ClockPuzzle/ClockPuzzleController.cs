using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockPuzzleController : MonoBehaviour
{
    public int statusCount;
    [SerializeField] GameObject reward;

    private void Start()
    {
        statusCount = 1;
    }

    public void CheckStatus()
    {
        if(statusCount == 4)
        {
            Debug.Log("Finish game");
            reward.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
}
