﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerAim : MonoBehaviour
{
    public Transform headPos;
    public float pickUpRange = 3f;

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, pickUpRange))
        {
            //Debug.DrawRay(headPos.position, headPos.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

            float distance = Vector3.Distance(transform.position, hit.transform.position);
            if (distance <= 3f)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Interact(hit);
                }
            }
        }
    }

    void Interact(RaycastHit hit)
    {
        if (hit.transform.GetComponent<KeypadKey>() != null)
        {
            hit.transform.GetComponent<KeypadKey>().SendKey();
        }

        //for arrow box puzzle
        if (hit.transform.GetComponent<ArrowBox>() != null)
        {
            hit.transform.GetComponent<ArrowBox>().RotateBox();
        }

        //for clock puzzle
        if (hit.transform.GetComponent<ClockInteract>() != null)
        {
            hit.transform.GetComponent<ClockInteract>().UpdateClockTime();
        }

        //for switch box puzzle
        if (hit.transform.GetComponent<SwitchBox>() != null)
        {
            hit.transform.GetComponent<SwitchBox>().ClickBox();
        }

        if (hit.transform.GetComponent<ResetBox>() != null)
        {
            hit.transform.GetComponent<ResetBox>().ResetPuzzle();
        }
    }
}

